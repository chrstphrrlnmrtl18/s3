<?php

// Objects as Variables
$buildingObject = (object) [
	'name' => 'Caswynn Buiding',
	'floors' => 8,
	'address' => (object) [
			'barangay' => 'Sacred heart',
			'city' => 'Quezon City',
			'country' => 'Philippines;'
		]
];

// Objects from clasess

class Building {
	protected $name;
	public $floors;
	public $address;

	// Constructor

	public function __construct($name, $floors, $address){

		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// method
	public function printName(){
		return "The name of the buidling is $this->name.";
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

// Inheritance and polymorphism
class Condominium extends Building {
	public function printName(){
		return "The name of the condominium is $this->name.";

	
	}
	// getters (read-only) and setters (write-only)
	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this-> name = $name;
	}

}

// instance

$condominium = new Condominium('Enzo Condo', 5, 'Buedia Avenue, Makati City, Philippines');


// Start of activity 3

class Person{
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		
		$this->firstName = $firstName;
		$this ->middleName= $middleName;
		$this ->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

$firstPerson = new Person('Chris', 'Orlina', 'Mortel');

class Developer extends Person{
	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName and your are a developer.";
	}
}

$firstDeveloper = new Developer('Daisy', 'Adsuara', 'dela Cruz');

class Engineer extends Person{
	public function printName(){
		return "Your are engineer named  $this->firstName $this->middleName $this->lastName.";
	}
}

$firstEngineer = new Engineer('Christopher', 'Orlina', 'Mortel');

?>