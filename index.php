<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S3: Classes and Objects, Inheritance and Polymorphism/S4: Access modifiers and Encapsulation</title>

</head>
<body>
	<h1>Objects from Variable</h1>

	<p><?php echo $buildingObject-> name; ?></p>


	<h1>Objects From Classes</h1>
	<pre>
		<?php print_r($building->printName()); ?>
	</pre>

	<pre>
		<?php print_r($condominium->printName()); ?>
	</pre>


	<h1>Start of Access Modifiers and Encapsulation</h1>

	<h2>Building Variables</h2>
	<p><?php //echo $building->name;?></p>

	<h2>CondominiumVariables</h2>
	<p><?php //echo $condominium->name;?></p>

	<h1>Encapsulation</h1>
	<p>
		The name of the condominium is
		<?php echo $condominium->getName(); ?>
	</p>

	<?php $condominium->setName('Enzo Tower'); ?>
	<p>
		The name of the condo has been changed to <?php echo $condominium->getName(); ?>
	</p>

	<!-- Start of activity 3 -->

	<p>
		<?php echo $firstPerson->printName(); ?>
	</p>

	<p>
		<?php echo $firstDeveloper->printName(); ?>
	</p>

	<p>
		<?php echo $firstEngineer->printName(); ?>
	</p>


</body>
</html>